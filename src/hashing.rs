use std::error::Error;
use std::fmt::{Display, Formatter};
use std::ops::Not;

pub fn create_hash_slices<const S: usize, const BS: usize>(
    reverse_dict: &[&str; S],
) -> Vec<Vec<u8>> {
    let mut hashes = vec![Vec::new(); BS];
    for (idx, rcb) in reverse_dict.iter().enumerate() {
        let rcb = rcb.as_bytes();
        let mut h2 = (rcb[0] << 3) as usize;
        let h1 = h2;
        if rcb.len() > 1 {
            h2 += rcb[1] as usize;
        }
        let h3 = if rcb.len() > 2 {
            h2 ^ rcb[2] as usize
        } else {
            0
        };

        if rcb.len() == 1 {
            let vc = &mut hashes[h1 as usize % 241];
            vc.push(rcb.len() as u8);
            vc.append(&mut rcb.to_vec());
            vc.push(idx as u8);
        } else if rcb.len() == 2 {
            let vc = &mut hashes[h2 as usize % 241];
            vc.push(rcb.len() as u8);
            vc.append(&mut rcb.to_vec());
            vc.push(idx as u8);
        } else if rcb.len() > 2 {
            let vc = &mut hashes[h3 as usize % 241];
            vc.push(rcb.len() as u8);
            vc.append(&mut rcb.to_vec());
            vc.push(idx as u8);
        }
    }

    return hashes;
}

pub fn find_in_hashes<I: AsRef<[u8]> + ?Sized>(
    input: &I,
    hashes: &Vec<Vec<u8>>,
) -> Result<usize, FindInHashesError> {
    let input = input.as_ref();
    if input.len() > u8::MAX as usize {
        return Err(FindInHashesError::InputTooBig);
    }

    let idx = match input.len() {
        0 => return Err(FindInHashesError::EmptyInput),
        1 => hash_one(input),
        2 => hash_two(input),
        _ => hash_three(input),
    };

    let mut found = &(hashes
        .get(idx)
        .ok_or_else(|| FindInHashesError::BucketNotFound)?)[..];
    let found = &mut found;

    while found.is_empty().not() {
        if found[0] as usize == input.len() {
            let idx = found[found[0] as usize + 1] as usize;
            return Ok(idx);
        } else {
            *found = &mut &found[found[0] as usize + 2..];
        }
    }

    return Err(FindInHashesError::EntryNotFound);
}

const fn hash_one(slice: &[u8]) -> usize {
    (slice[0] << 3) as usize
}

const fn hash_two(slice: &[u8]) -> usize {
    hash_one(slice) + slice[1] as usize
}

const fn hash_three(slice: &[u8]) -> usize {
    hash_two(slice) ^ slice[2] as usize
}

#[derive(Debug, Clone, Copy)]
pub enum FindInHashesError {
    EmptyInput,
    InputTooBig,
    BucketNotFound,
    EntryNotFound,
}

impl Display for FindInHashesError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            FindInHashesError::EmptyInput => write!(f, "Empty input"),
            FindInHashesError::InputTooBig => {
                write!(f, "input is too big, it will not be in the hash table")
            }
            FindInHashesError::BucketNotFound => write!(f, "bucket was not found"),
            FindInHashesError::EntryNotFound => {
                write!(f, "bucket was found, but the entry was not")
            }
        }
    }
}

impl Error for FindInHashesError {}
