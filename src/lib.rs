//! # Fast smaz
//!
//! Fast and pure Rust port of [antirez/smaz](https://github.com/antirez/smaz) library.
//!
//! # Example
//!
//! ```rust
//! use fast_smaz::Smaz;
//! let my_message = "Hello world!";
//! let compressed = my_message.smaz_compress();
//! let decompressed = compressed.smaz_decompress().unwrap();
//! assert_eq!(my_message.as_bytes(), decompressed);
//! ```
//!
//! # Unicode/ASCII compatibility
//!
//! **fast-smaz** does not do any assumption on whether the input is encoded using Unicode or ASCII,
//! the entire API is designed to work only with byte slices ([`&[u8]`][`u8`]). However, the original smaz
//! cookbook uses only ASCII encoded strings, so it will only really **compress** the data (given the codebook words)
//! if the input encoding is a superset of the ASCII encoding, which includes UTF-8 (but not UTF-16 or UTF-32).
//!
//! However, trying to compress strings encoded with anything other than ASCII or UTF-8 is not guaranteed
//! to actually compress the input string, but it is expected to at least correctly encode the input
//! data conforming to SMAZ spec. Which means you still can get back the original data, but it will
//! probably never compress the string.
//!
//! This also means that, you can pass non-UTF8 and non-ASCII encoded inputs, get back the original data
//! and recreate the string using the same input encoding. This is guaranteed to be safe as long as
//! the data passed to the `decompress` conforms to SMAZ spec and was compressed using a SMAZ compression
//! algorithm.
//!
//! ## Compressing non-ASCII characters (2-byte, 3-byte or 4-byte wide)
//!
//! Since the original codebook only has ASCII characters, non-ASCII characters are just stored as is
//! in the result data, and restored back when decompressed. Nothing is lost when using SMAZ, even if
//! it cannot be compressed or even if it is a sequence of bytes that is not a valid character in any
//! existing encoding.
//!
//! # How smaz works
//!
//! Here we dive into SMAZ algorithm details.
//!
//! ## The codebook and reverse codebook
//!
//! All the process is very simple once you understand the structure and the logic of SMAZ algorithm,
//! below I've described how the algorithm works. This can be used as a resource to understand the algorithm,
//! write a new implementation of it or to use for anything useful.
//!
//! ### Compression
//!
//! The [codebook][crate::SMAZ_CB] is a simple hash table with multiple buckets and provides fast-lookup
//! for substrings that can be compressed.
//!
//! It is basically constructed in this form:
//!
//! An entry is consisted of:
//!
//! ```text
//! [NUMBER_OF_BYTES][BYTES_OF_ASCII_STRING][INDEX_IN_REVERSE_CODEBOOK]
//! ```
//!
//! And a bucket is just a sequence of entries, like this:
//!
//! ```text
//! [ENTRY][ENTRY][ENTRY]...
//! ```
//!
//! There is no separator character between entries, it is solely based on the fact that the `NUMBER_OF_BYTES`
//! occupies a single byte (`u8`), followed by N bytes, which is the value of `NUMBER_OF_BYTES`
//! and a final value `INDEX_IN_REVERSE_CODEBOOK` which occupies a single byte as well (`u8`).
//!
//! Because of this, you can seek to any entry in the bucket by looking at the previous one, which gives
//! the well known time complexity of `O(n)` for searching an entry inside a bucket of a hash table
//! (but instead of a linked-list, we have a contiguous sequence of data, an array).
//!
//! Every element in [codebook][crate::SMAZ_CB] array (i.e. every slice) is a bucket,
//! and we can use the original SMAZ hashing algorithm to find buckets that probably contains
//! the subslice we are looking for.
//!
//! #### Bucket example
//!
//! For example, given the words `foo` and `bar`, considering they reside in the same bucket and
//! are located in the index `4` and `5` of the [reverse codebook][crate::SMAZ_RCB], respectively:
//!
//! ```text
//! [3, 'f', 'o', 'o', 4, 3, 'b', 'a', 'r', 5]
//! ```
//!
//! #### Hash table example
//!
//! The hash table must follow the distribution based on the hashing algorithm used to find the buckets,
//! which is hard-coded in the SMAZ algorithm. SMAZ uses 3 different algorithms, one for subslices
//! with only one element, one for subslices with 2 elements and one for subslices with 3 or more elements.
//!
//! Each bucket is one element in the [codebook][crate::SMAZ_CB] array, for example:
//!
//! ```rust,ignore
//! [
//!     &[3, 'f', 'o', 'o', 4, 3, 'b', 'a', 'r', 5], // first bucket
//!     &[3, 'b', 'a', 'z', 6],                      // second bucket
//! ]
//! ```
//!
//! #### What if we don't find any entry?
//!
//! In this case, we put the byte in a buffer and process the next one, trying to find a subslice
//! in the hash table.
//!
//! And now we have two scenarios, if we find an entry, the previous non-encoded byte follows after
//! a `\254` code, for example, let's say that the `byte 54` is not encoded and is in the queue,
//! we push `[\254, 54]` to the output result vector.
//!
//! If we don't find an entry, we repeat the same process, pushing the byte to the buffer and processing
//! the next one, now, if we do find an entry, we have more than one byte in our queue, so both of them
//! follows after a `\255` code and an additional length byte, for example, let's say that there is
//! the `byte 54` and `byte 88` in the queue, we push `[\255, 1, 54, 88]` to the output result vector,
//! if we had three bytes, the previous ones plus `44`, we push `[\255, 2, 54, 88, 44]`. Note that
//! the second value is the number of additional bytes, we already know that there must have a single
//! byte after the length, because otherwise it would be encoded with `\254`, so we do push
//! `N - 1` as the number of additional bytes, instead of `N`.
//!
//! Why? this allows we to store one additional byte before we reach the max values of `u8` ([`u8::MAX`]).
//!
//! In this way, we can also preserve the input integrity even if it cannot be compressed.
//!
//! Also, one last important information, the maximum subslice length we look for is `7`, which is the
//! size of the longest string in the codebook (`http://`), and the minimum is `1`.
//!
//! And, empty inputs translates into empty outputs, doesn't matter if you pass them to `compress`, `encode`
//! or `decompress`.
//!
//! Note that we reslice the input data to skip the bytes we already processed, which happens
//! when we find an entry or when we feed the byte(s) to the buffer.
//!
//! ### Decompression
//!
//! For decompression, we use the [reverse codebook][crate::SMAZ_RCB], we take the first character and check:
//!
//! - If it is `\254`, take the next value and push to the result vector.
//! - If it is `\255`, take the next value, which is the number of additional bytes to read (let assign to `N`),
//!   then read the next byte (right after the size) and push to the result vector, then take the next `N` and
//!   push all them to the result vector.
//! - Otherwise, use the byte as the index to lookup at the [reverse codebook][crate::SMAZ_RCB], and
//!   push all the characters found in the [reverse codebook][crate::SMAZ_RCB] at the provided index
//!   to the result vector.
//!
//! Note that, after one of those steps, we take a new slice of the input encoded data starting right
//! after the last processed byte, for example, for the first case, we start after the first 2 bytes,
//! for the second one, we start after the first 3 bytes (`\255`, length and a single byte) plus the additional
//! read bytes, and for the third one we start after only one byte.

use std::cmp::min;
use std::io::ErrorKind;
use std::ops::Not;

#[cfg(feature = "custom-cookbook")]
mod custom_cookbook;
#[cfg(feature = "hashing")]
pub mod hashing;
#[cfg(not(feature = "custom-cookbook"))]
mod original_cookbook;

#[cfg(feature = "custom-cookbook")]
pub use custom_cookbook::SMAZ_CB;
#[cfg(not(feature = "custom-cookbook"))]
pub use original_cookbook::SMAZ_CB;

/// Reverse cookbook taken from: [antirez/smaz](https://github.com/antirez/smaz/blob/master/smaz.c#L55)
pub const SMAZ_RCB: [&str; 254] = [
    " ", "the", "e", "t", "a", "of", "o", "and", "i", "n", "s", "e ", "r", " th", " t", "in", "he",
    "th", "h", "he ", "to", "\r\n", "l", "s ", "d", " a", "an", "er", "c", " o", "d ", "on", " of",
    "re", "of ", "t ", ", ", "is", "u", "at", "   ", "n ", "or", "which", "f", "m", "as", "it",
    "that", "\n", "was", "en", "  ", " w", "es", " an", " i", "\r", "f ", "g", "p", "nd", " s",
    "nd ", "ed ", "w", "ed", "http://", "for", "te", "ing", "y ", "The", " c", "ti", "r ", "his",
    "st", " in", "ar", "nt", ",", " to", "y", "ng", " h", "with", "le", "al", "to ", "b", "ou",
    "be", "were", " b", "se", "o ", "ent", "ha", "ng ", "their", "\"", "hi", "from", " f", "in ",
    "de", "ion", "me", "v", ".", "ve", "all", "re ", "ri", "ro", "is ", "co", "f t", "are", "ea",
    ". ", "her", " m", "er ", " p", "es ", "by", "they", "di", "ra", "ic", "not", "s, ", "d t",
    "at ", "ce", "la", "h ", "ne", "as ", "tio", "on ", "n t", "io", "we", " a ", "om", ", a",
    "s o", "ur", "li", "ll", "ch", "had", "this", "e t", "g ", "e\r\n", " wh", "ere", " co", "e o",
    "a ", "us", " d", "ss", "\n\r\n", "\r\n\r", "=\"", " be", " e", "s a", "ma", "one", "t t",
    "or ", "but", "el", "so", "l ", "e s", "s,", "no", "ter", " wa", "iv", "ho", "e a", " r",
    "hat", "s t", "ns", "ch ", "wh", "tr", "ut", "/", "have", "ly ", "ta", " ha", " on", "tha",
    "-", " l", "ati", "en ", "pe", " re", "there", "ass", "si", " fo", "wa", "ec", "our", "who",
    "its", "z", "fo", "rs", ">", "ot", "un", "<", "im", "th ", "nc", "ate", "><", "ver", "ad",
    " we", "ly", "ee", " n", "id", " cl", "ac", "il", "</", "rt", " wi", "div", "e, ", " it",
    "whi", " ma", "ge", "x", "e c", "men", ".com",
];

/// Compresses the input using [SMAZ algorithm](https://github.com/antirez/smaz).
///
/// SMAZ is suitable only for english short strings.
pub fn compress<D: AsRef<[u8]> + ?Sized>(input: &D) -> Vec<u8> {
    compress_::<D, true>(input)
}

/// Encodes the string to conform to [SMAZ spec](https://github.com/antirez/smaz), but do not
/// compress it.
///
/// Passing the result to any SMAZ decoder is guaranteed to succeed and return the original string.
/// However, the encoded result will be larger than the original input.
pub fn encode<D: AsRef<[u8]> + ?Sized>(input: &D) -> Vec<u8> {
    compress_::<D, false>(input)
}

fn compress_<D: AsRef<[u8]> + ?Sized, const COMPRESS: bool>(input: &D) -> Vec<u8> {
    let mut input = input.as_ref();

    if input.len() == 0 {
        return Vec::new();
    }

    let mut out = Vec::with_capacity(input.len() / 2);
    let mut verb = Vec::with_capacity(256);
    let mut queue = Vec::with_capacity(input.len());
    let flush = |verb: &mut Vec<u8>, queue: &mut Vec<u8>| match verb.len() {
        0 => {}
        1 => {
            queue.push(254u8);
            queue.append(verb);
        }
        n @ _ => {
            queue.push(255u8);
            queue.push((n - 1) as u8);
            queue.append(verb);
        }
    };

    while input.is_empty().not() {
        let h1 = (input[0] as usize) << 3;
        let h2 = if input.len() > 1 {
            h1 + (input[1] as usize)
        } else {
            h1
        };
        let h3 = if input.len() > 2 {
            h2 ^ (input[2] as usize)
        } else {
            0
        };

        if COMPRESS {
            'outer: for j in (1..=min(input.len(), 7)).rev() {
                let mut slot = match j {
                    1 => SMAZ_CB.get(h1 % 241).copied(),
                    2 => SMAZ_CB.get(h2 % 241).copied(),
                    _ => SMAZ_CB.get(h3 % 241).copied(),
                };

                while let Some(slot) = slot.as_mut() {
                    if slot.is_empty() {
                        break;
                    }

                    if slot[0] as usize == j && slot.get(1..j + 1) == input.get(0..j) {
                        flush(&mut verb, &mut queue);

                        queue.push(slot[slot[0] as usize + 1] as u8);
                        input = &input[j..];
                        break 'outer;
                    } else {
                        // This is safe as long as we don't allow this section to be reached without
                        // checking if !slot.is_empty()
                        *slot = &mut &slot[slot[0] as usize + 2..];
                    }
                }
            }
        }

        // Why? Why did I used a tuple?
        if let (true, Some(first_byte)) = (queue.is_empty(), input.get(0)) {
            verb.push(*first_byte);
            input = &input[1..];
        }

        if verb.len() == 256 || (verb.is_empty().not() && input.is_empty()) {
            flush(&mut verb, &mut queue);
        }

        out.append(&mut queue);
    }

    out
}

/// Decompress the input using [SMAZ algorithm](https://github.com/antirez/smaz).
///
/// This function may fail if the `input` is not a SMAZ compliant input or is corrupted.
pub fn decompress<D: AsRef<[u8]> + ?Sized>(input: &D) -> std::io::Result<Vec<u8>> {
    let mut input = input.as_ref();

    if input.len() == 0 {
        return Ok(Vec::new());
    }

    let mut out = Vec::with_capacity(input.len() * 3);

    while let Some(c) = input.get(0) {
        match *c {
            255 => {
                let size = *input.get(1).ok_or_else(|| {
                    std::io::Error::new(ErrorKind::InvalidData, "expected length after \\255 byte")
                })? as usize;
                let size = size + 1;

                let values = input.get(2..size + 2).ok_or_else(|| {
                    std::io::Error::new(
                        ErrorKind::InvalidData,
                        format!("expected {} values after \\255 byte", size),
                    )
                })?;

                // This is faster than: `out.extend(values);`,
                // because `extend` uses `copy_nonoverlapping` and it's slower than
                // should be. This should fix it: https://github.com/rust-lang/rust/pull/83785.

                // Not really needed, capacity growth is very rare.
                out.reserve(values.len());
                for b in values {
                    out.push(*b);
                }

                // Once `https://github.com/rust-lang/rust/pull/83785` is merged,
                // this should be preferred because:
                // - it avoids capacity check for every element
                // - extend will know the exact needed capacity to insert all elements
                // - it copies the slice elements directly to the Vec
                //out.extend(values);
                input = &input[size + 2..];
            }
            254 => {
                if let Some(b) = input.get(1) {
                    out.push(*b);
                } else {
                    return Err(std::io::Error::new(
                        ErrorKind::InvalidData,
                        "expected a value after \\254 byte",
                    ));
                }

                input = &input[2..];
            }
            _ => {
                // Safe because SMAZ_RCB has 254 elements, from 0..=253
                // and both bytes 254 and 255 are already handled by the others branches.
                let data = SMAZ_RCB[*c as usize];
                let values = data.as_bytes();

                // This is faster than: `out.extend(values);`,
                // because `extend` uses `copy_nonoverlapping` and it's slower than
                // should be. This should fix it: https://github.com/rust-lang/rust/pull/83785.

                // Not really needed, capacity growth is very rare.
                out.reserve(values.len());
                for b in values {
                    out.push(*b);
                }

                // Once `https://github.com/rust-lang/rust/pull/83785` is merged,
                // this should be preferred because:
                // - it avoids capacity check for every element
                // - extend will know the exact needed capacity to insert all elements
                // - it copies the slice elements directly to the Vec
                // - reduces the number of instructions, L1 cache and RAM accesses
                //   and number of cycles.
                //out.extend(values);
                input = &input[1..];
            }
        }
    }

    return Ok(out);
}

/// Provides extension functions to compress and decompress using SMAZ algorithm.
pub trait Smaz {
    fn smaz_compress(&self) -> Vec<u8>;
    fn smaz_encode(&self) -> Vec<u8>;
    fn smaz_decompress(&self) -> std::io::Result<Vec<u8>>;
}

impl<T> Smaz for T
where
    T: AsRef<[u8]>,
{
    /// Read [`crate::compress`]
    fn smaz_compress(&self) -> Vec<u8> {
        compress(self)
    }

    /// Read [`crate::encode`]
    fn smaz_encode(&self) -> Vec<u8> {
        encode(self)
    }

    /// Read [`crate::decompress`]
    fn smaz_decompress(&self) -> std::io::Result<Vec<u8>> {
        decompress(self)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::{assert_eq, assert_ne};

    /// Creates a test case for every string, testing if it was really compressed when [`crate::compress`] is called,
    /// and if it is not when [`crate::encode`] is called.
    ///
    /// And checking if value produced by both [`crate::compress`] and
    /// [`crate::encode`] translates into the same original value when provided to [`crate::decode`].
    ///
    /// It also checks if the value produced by [`crate::encode`] is smaller or bigger than the original,
    /// or has the same size, through `[compressed: true]`, `[compressed: false]` and `[unchanged: true]`,
    /// respectively.
    macro_rules! test_string {
        ($($([compressed: $compressed:literal])? $([unchanged: $unchanged:literal])? $name:ident -> $li:literal),+) => {
            $(
                #[test]
                fn $name() {
                    let s = $li;
                    let compressed_bytes = compress(s);
                    let encoded_bytes = encode(s);

                    $(
                        if $compressed {
                            assert!(compressed_bytes.len() < s.len(), "compressed bytes `{:?}` is not smaller than original string `{:?}`", compressed_bytes, s.as_bytes());
                        } else {
                            assert!(compressed_bytes.len() > s.len(), "compressed bytes `{:?}` must be larger than original string `{:?}`", compressed_bytes, s.as_bytes());
                        }
                    )?

                    $(
                        if $unchanged {
                            assert_eq!(compressed_bytes.len(), s.len(), "compressed bytes `{:?}` must have the same size as original string `{:?}`", compressed_bytes, s.as_bytes());
                        } else {
                            assert_ne!(compressed_bytes.len(), s.len(), "compressed bytes `{:?}` must not have the same size as the original string `{:?}`", compressed_bytes, s.as_bytes());
                        }
                    )?

                    assert_eq!(
                        is_really_compressed(&compressed_bytes),
                        true,
                        "the value was not compressed: `{:?}`, for string `{:?}`",
                        compressed_bytes,
                        s.as_bytes(),
                    );

                    if s.len() > 0 {
                        assert_eq!(
                            is_really_compressed(&encoded_bytes),
                            false,
                            "the value was compressed when it shouldn't: `{:?}`, for string: `{:?}`",
                            encoded_bytes,
                            s.as_bytes(),
                        );
                        assert_eq!(
                            encoded_bytes.len(),
                            s.len() + 2,
                            "encoded bytes (left) expected to be greater than original string (right). left: `{encoded_bytes:?}`, right: `{:?}`",
                            s.as_bytes()
                        );
                    }

                    let decompressed_string = decompress(&compressed_bytes)
                        .and_then(|d| {
                            String::from_utf8(d).map_err(|e| {
                                std::io::Error::new(ErrorKind::InvalidData, format!("{}", e))
                            })
                        })
                        .map_err(|e| format!("{}", e));

                    let decoded_string = decompress(&encoded_bytes)
                        .and_then(|d| {
                            String::from_utf8(d).map_err(|e| {
                                std::io::Error::new(ErrorKind::InvalidData, format!("{}", e))
                            })
                        })
                        .map_err(|e| format!("{}", e));

                    assert_eq!(
                        Ok(s.to_string()),
                        decompressed_string,
                        "decompressed string `{:?}` is not the same as original string `{:?}`.",
                        decompressed_string,
                        s
                    );

                    assert_eq!(
                        Ok(s.to_string()),
                        decoded_string,
                        "decoded string `{:?}` is not the same as original string `{:?}`.",
                        decoded_string,
                        s
                    );
                }
            )+
        };
    }

    test_string!(
        [unchanged: true] empty_string -> "",
        [compressed: true] small_string -> "This is a small string",
        [compressed: true] foobar -> "foobar",
        [compressed: true] the_end -> "the end",
        [compressed: false] bad_example -> "not-a-g00d-Exampl333",
        [compressed: true] smaz_text -> "Smaz is a simple compression library",
        [compressed: true] long_text -> "Nothing is more difficult, and therefore more precious, than to be able to decide",
        [compressed: true] very_good_example -> "this is an example of what works very well with smaz",
        [compressed: true] numeric_example -> "1000 numbers 2000 will 10 20 30 compress very little",
        [compressed: true] ital_sentence_start -> "and now a few italian sentences:",
        [compressed: true] ital_sentence_1 -> "Nel mezzo del cammin di nostra vita, mi ritrovai in una selva oscura",
        [compressed: true] ital_sentence_2 -> "Mi illumino di immenso",
        [compressed: true] ital_sentence_3 -> "L'autore di questa libreria vive in Sicilia",
        [compressed: true] url_example_start -> "try it against urls",
        [compressed: true] google_url -> "http://google.com",
        [compressed: true] programming_reddit_url -> "http://programming.reddit.com",
        [compressed: true] additional_example_1 -> "rust programming language is fast and extremely fun",
        [compressed: true] additional_example_2 -> "why borrow my heart, when you can move and own it",
        [compressed: true] additional_example_3 -> "I'm not sure if I'm sure",
        [compressed: true] additional_example_4 -> "Read it, Readit, Reddit",
        [compressed: true] additional_example_5 -> "zmas rof doog ton si elpmaxe sihT",
        [compressed: true] additional_example_unicode -> "This is a ¥ string",
        [compressed: true] additional_example_pt_br_1 -> "Olá mundo, Rust é uma linguagem incrível",
        [compressed: true] additional_example_pt_br_2 -> "Rust despertou ascendeu a chama do meu amor por programação",
        [compressed: false] additional_example_pt_br_3 -> "Ñãõ śéŕá ṕóśśíṽél ćõḿṕŕíḿíŕ ćóḿ áćéńẗóś."
    );

    fn is_really_compressed(compressed: &[u8]) -> bool {
        if compressed.len() != 0
            && compressed[0] == 255
            && compressed.len() > 2
            && compressed[1] as usize == (compressed.len() - 3)
        {
            false
        } else {
            true
        }
    }
}
