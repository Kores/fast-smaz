use iai::black_box;

const BENCH_STRINGS: [&str; 22] = [
    "",
    "This is a small string",
    "foobar",
    "the end",
    "not-a-g00d-Exampl333",
    "Smaz is a simple compression library",
    "Nothing is more difficult, and therefore more precious, than to be able to decide",
    "this is an example of what works very well with smaz",
    "1000 numbers 2000 will 10 20 30 compress very little",
    "and now a few italian sentences:",
    "Nel mezzo del cammin di nostra vita, mi ritrovai in una selva oscura",
    "Mi illumino di immenso",
    "L'autore di questa libreria vive in Sicilia",
    "try it against urls",
    "http://google.com",
    "http://programming.reddit.com",
    "rust programming language is fast and extremely fun",
    "why borrow my heart, when you can move and own it",
    "I don't know what I'm doing",
    "I'm not sure what I'm doing",
    "Read it, Readit, Reddit",
    "zmas rof doog ton si elpmaxe sihT",
];

const BENCH_COMPRESSED: [&[u8]; 16] = [
    &[],
    &[254, 84, 76, 56, 172, 62, 173, 152, 62, 195, 70],
    &[220, 6, 90, 79],
    &[1, 171, 61],
    &[
        132, 204, 4, 204, 59, 255, 1, 48, 48, 24, 204, 254, 69, 250, 4, 45, 60, 22, 255, 2, 51, 51,
        51,
    ],
    &[
        254, 83, 173, 219, 56, 172, 62, 226, 60, 87, 161, 45, 60, 33, 166, 107, 205, 8, 90, 130,
        12, 83,
    ],
    &[
        254, 78, 223, 102, 99, 116, 45, 42, 11, 129, 44, 44, 131, 38, 22, 3, 148, 63, 210, 68, 11,
        45, 42, 11, 60, 33, 28, 144, 164, 36, 203, 143, 96, 92, 25, 90, 87, 82, 165, 215, 237, 2,
    ],
    &[
        155, 56, 172, 41, 2, 250, 4, 45, 60, 87, 32, 159, 135, 65, 42, 254, 107, 23, 231, 71, 145,
        152, 243, 227, 10, 173, 219,
    ],
    &[
        255, 3, 49, 48, 48, 48, 236, 38, 45, 92, 221, 0, 255, 3, 50, 48, 48, 48, 243, 152, 0, 255,
        1, 49, 48, 0, 255, 1, 50, 48, 0, 255, 1, 51, 48, 161, 45, 60, 33, 166, 0, 231, 71, 151, 3,
        3, 87,
    ],
    &[
        7, 236, 6, 65, 146, 44, 2, 65, 246, 88, 8, 26, 62, 97, 51, 136, 10, 254, 58,
    ],
    &[
        254, 78, 178, 123, 2, 219, 219, 96, 106, 180, 28, 4, 45, 45, 105, 129, 236, 6, 77, 130, 0,
        109, 47, 4, 36, 45, 8, 189, 47, 115, 109, 4, 8, 78, 0, 224, 163, 95, 22, 109, 163, 6, 10,
        28, 150, 4,
    ],
    &[254, 77, 8, 56, 152, 38, 45, 15, 96, 129, 56, 45, 252, 179],
    &[
        255, 1, 76, 39, 4, 196, 42, 11, 129, 0, 254, 113, 38, 54, 200, 205, 8, 90, 33, 114, 163,
        109, 186, 11, 105, 254, 83, 131, 240, 8, 4,
    ],
    &[195, 71, 47, 25, 59, 4, 15, 77, 0, 150, 22, 10],
    &[67, 59, 6, 6, 59, 87, 253],
    &[67, 60, 115, 59, 130, 45, 45, 70, 110, 33, 24, 129, 3, 253],
];

fn fast_smaz_compress() {
    for s in BENCH_STRINGS {
        black_box(fast_smaz::compress(black_box(s)));
    }
}

fn rust_smaz_compress() {
    for s in BENCH_STRINGS {
        black_box(smaz::compress(black_box(s.as_bytes())));
    }
}

fn fast_smaz_decompress() {
    for s in BENCH_COMPRESSED {
        black_box(fast_smaz::decompress(black_box(s)).unwrap());
    }
}

fn rust_smaz_decompress() {
    for s in BENCH_COMPRESSED {
        black_box(smaz::decompress(black_box(s)).unwrap());
    }
}

iai::main!(
    fast_smaz_compress,
    rust_smaz_compress,
    fast_smaz_decompress,
    rust_smaz_decompress
);
